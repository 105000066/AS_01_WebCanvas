var save = [];
var restore = [];
var drawingSurfaceImageData;
var drawing;
var index = 0;
var rindex = 0;
var modeButtons = document.querySelectorAll(".mode");

modeButtons[0].classList.add('selected');

(function initModeButtons() {
    for (var i = 0; i < modeButtons.length; i++) {
        modeButtons[i].addEventListener("click", function() {
			if(this.textContent !== 'undo'&& this.textContent !== 'redo'&& this.textContent !== 'reset')
				for(var j=0 ; j<6; j++)
					modeButtons[j].classList.remove('selected');
            if(this.textContent === 'pencil')
            {
				active_tool = 'pencil';
				this.classList.add('selected');
            }    
            else if(this.textContent === 'eraser')
            {
				active_tool = "eraser";
				this.classList.add('selected');
            }
            else if(this.textContent === 'rectangle')
            {
				active_tool = "rectangle";
				this.classList.add('selected');
			}
			else if(this.textContent === 'circle')
            {
				active_tool = "circle";
				this.classList.add('selected');
			}
			else if(this.textContent === 'triangle')
            {
				active_tool = "triangle";
				this.classList.add('selected');
			}
			else if(this.textContent === 'text')
            {
				active_tool = "text";
				this.classList.add('selected');
			}
			else if(this.textContent === 'undo')
            {
				drawing = context_o.getImageData(0, 0,
					canvas.width,
					canvas.height);
				restore[rindex] = drawing;
				rindex++;

				context.clearRect(0,0,canvas_temp.width, canvas_temp.height);
				context_o.clearRect(0,0,canvas.width,canvas.height);
				index = index - 2;
				drawingSurfaceImageData = save[index];
				context.putImageData(drawingSurfaceImageData, 0, 0);
				index++;
				return;
			}
			else if(this.textContent === 'redo')
			{
				context.clearRect(0,0,canvas_temp.width, canvas_temp.height);
				context_o.clearRect(0,0,canvas.width,canvas.height);
				rindex = rindex - 1;
				drawing = restore[rindex];
				context.putImageData(drawing, 0, 0);
				rindex++;

				drawingSurfaceImageData = context_o.getImageData(0, 0,
					canvas.width,
					canvas.height);
				save[index] = drawing;
				index++;
			}
			else if(this.textContent === 'reset')
			{
				context.clearRect(0,0,canvas_temp.width, canvas_temp.height);
				context_o.clearRect(0,0,canvas.width,canvas.height);
			}
        });
    }
})();

var canvas = document.getElementById("maincanvas");
var context_o = canvas.getContext("2d");
var color;
var linewidth = 2;



function myFunction()
{
	linewidth = document.getElementById("ws").value;
}

var active_tool = "pencil";
var container = canvas.parentNode;
canvas_temp = document.createElement('canvas');
canvas_temp.id = "c_temp";
canvas_temp.width = canvas.width;
canvas_temp.height = canvas.height;
container.appendChild(canvas_temp);
context = canvas_temp.getContext("2d");

canvas_temp.addEventListener('mousedown', position, false);
canvas_temp.addEventListener('mousemove', position, false);
canvas_temp.addEventListener('mouseup',	 position, false);

function setcursor()
{
	if(active_tool === "pencil") canvas_temp.style.cursor = "url(Pen.cur),auto";
	else if(active_tool === "eraser")canvas_temp.style.cursor = "url(eraser.cur),auto";
	else if(active_tool === "text") canvas_temp.style.cursor = "text";
	else canvas_temp.style.cursor = "url(cross_hair.cur),auto";	
};

function draw()
{
	context_o.drawImage(canvas_temp,0,0);
	context.clearRect(0,0,canvas_temp.width, canvas_temp.height);
	drawingSurfaceImageData = context_o.getImageData(0, 0,
		canvas.width,
		canvas.height);
	save[index] = drawingSurfaceImageData;
	index++;
}

function position(ev)
{
  var rect = canvas_temp.getBoundingClientRect();
  ev._x = ev.clientX - rect.left;
  ev._y = ev.clientY - rect.top;  
  var func=TOOL[ev.type];
  if(func)
  {
	func(ev);
	setcursor();
  }
}

(function fillbackground()
{
	context.fillStyle = "white";
	context.fillRect(0, 0, canvas.width, canvas.height);
})();

TOOL = new tool_();

function tool_ () {
	var tool = this;
	this.started = false;

	this.mousedown = function (ev) {
		if(active_tool == "pencil")
		{
			context.beginPath();
			context.moveTo(ev._x, ev._y);
			tool.started = true;
		}
		else if(active_tool == "rectangle")
		{
			tool.started = true;
			tool.x0 = ev._x;
			tool.y0 = ev._y;
		}
		else if(active_tool == "eraser")
		{
			context.beginPath();
			context.moveTo(ev._x, ev._y);
			tool.started = true;
		}
		else if (active_tool == "circle")
		{
			tool.started = true;
			tool.x0 = ev._x;
			tool.y0 = ev._y;
			
		}	
		else if(active_tool == "triangle")
		{
			tool.started = true;
			tool.x0 = ev._x;
			tool.y0 = ev._y;
		}
		else if(active_tool == "text")
		{
			tool.started = true;
			tool.x0 = ev._x;
			tool.y0 = ev._y;
		}
	};

	this.mousemove = function (ev) {
		context.strokeStyle=color;
		context.lineWidth = linewidth;

		if(!tool.started) return;
		else if(active_tool == "pencil")
		{
			context.lineTo(ev._x, ev._y);
			context.stroke();
		}
		else if(active_tool == "rectangle")
		{
			var x = Math.min(ev._x , tool.x0);
			var y = Math.min(ev._y , tool.y0);
			var w = Math.abs(ev._x - tool.x0);
			var h = Math.abs(ev._y - tool.y0);
			if(!w || !h) return;
			context.clearRect(0, 0, canvas_temp.width, canvas_temp.height);
			context.strokeRect(x,y,w,h);
		}
		else if(active_tool == "eraser")
		{
			context.lineTo(ev._x, ev._y);
			context.strokeStyle="#ffffff";
			context.stroke();
		}
		else if (active_tool == "circle")
		{
			
			var x = Math.min(ev._x , tool.x0);
			var y = Math.min(ev._y , tool.y0);
			var w = Math.abs(ev._x - tool.x0);	
			if(!w) return;
			context.clearRect(0, 0, canvas_temp.width, canvas_temp.height);
			context.beginPath();
			context.arc(x,y,w,0,2*Math.PI);
			context.closePath();
			context.stroke();			
		}	
		else if(active_tool == "triangle")
		{
			var halfWidth = Math.abs(ev._x - tool.x0)/2;
			var xmin = Math.min(ev._x, tool.x0);
			var xmax = Math.max(ev._x, tool.x0);
			var ymin = Math.min(ev._y , tool.y0);
			var ymax = Math.max(ev._y , tool.y0);
			context.clearRect(0, 0, canvas_temp.width, canvas_temp.height);
			context.beginPath();
    		context.moveTo(xmin, ev._y - halfWidth); // Top
    		context.lineTo(xmin - halfWidth, ymax + halfWidth); // Bottom left
    		context.lineTo(xmax + halfWidth, ymax + halfWidth); // Bottom right
			context.lineTo(xmin, ev._y - halfWidth); // Back to Top
			context.closePath();
    		context.stroke();
		}
	};

	this.mouseup = function (ev) {
		if(!tool.started) return;
		if(active_tool == "text")
		{
			var message = prompt("Please enter text", "");
			if(message != null) 
			{
				var format = prompt("Please enter text format","20px Georgia");
				if(format != null) context.font = format;
				context.fillStyle = color;
				context.fillText(message, tool.x0, tool.y0);
			}
			tool.started = false;
			draw();
			return;
		}
		tool.mousemove(ev);
		tool.started = false;
		draw();
		context.strokeStyle="#000000";
	};
}


var colorWell;
var defaultColor = "#000000";
color = "#000000";

(function startup() {
	colorWell = document.querySelector("#colorWell");
	colorWell.value = defaultColor;
	colorWell.addEventListener("input", updateFirst, false);
	colorWell.addEventListener("change", updateAll, false);
	colorWell.select();
})();

function updateFirst(event) {

	if (p) {
	  color = event.target.value;
	}
  }

  function updateAll(event) {

	  color = event.target.value;
  }

var button = document.getElementById('btn-download');
button.addEventListener('click', function (e) {
    var dataURL = canvas.toDataURL('image/jpeg');
    button.href = dataURL;
});

var imageLoader = document.getElementById('imageLoader');
	imageLoader.addEventListener('change', handleImage, false);
	
	function handleImage(e){
		var reader = new FileReader();
		reader.onload = function(event){
			var img = new Image();
			img.onload = function(){
				context.drawImage(img,0,0);
			}
			img.src = event.target.result;
		}
		reader.readAsDataURL(e.target.files[0]);     
	}